import cv2
import numpy as np
import os

# La fonction facedetection ne sert qu'à vérifier à la main qu'un visage est reconnu (grâce à l'affichage).
# Pour l'appliquer, il vaut mieux sortir l'image en question du dossier. Les images de lfw sont en jpg,
# il faut alors décommenter la ligne commentée et commenter la précédente dans la fonction facedetection.


def facedetection(source_img):
    img = cv2.imread(source_img)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    face_cascade = cv2.CascadeClassifier(os.path.join(
        cv2.data.haarcascades, "haarcascade_frontalface_default.xml"))
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)
    i = 0
    for (x, y, w, h) in faces:
        cv2.imwrite(f'{source_img.rstrip(".png")}_face{i}.png',
                    cv2.resize(img[y:y+h, x:x+w, :], (224, 224)))
        # cv2.imwrite(f'{source_img.rstrip(".jpg")}_face{i}.jpg',cv2.resize(img[y:y+h, x:x+w, :], (224, 224)))
        i += 1
    cv2.imshow('window', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def facedetection_and_resizing(source_img):
    img = cv2.imread(source_img)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    face_cascade = cv2.CascadeClassifier(os.path.join(
        cv2.data.haarcascades, "haarcascade_frontalface_default.xml"))
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    i = 0
    list_faces = []
    for (x, y, w, h) in faces:
        list_faces.append(cv2.resize(img[y:y+h, x:x+w, :], (224, 224)))
        i += 1
    if len(list_faces) == 0:
        return None
    # Ici, il est possible que plusieurs visages soient reconnus sur une même photo, mais je n'affiche que le premier"
    return list_faces[0]


# facedetection('../dataset/angele/img_2.png')
