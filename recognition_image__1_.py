import numpy as np
from imagecollect.facedetection import facedetection_and_resizing
from imagecollect.database_building_and_update import dic
import cv2
from keras_vggface import VGGFace
import os
import tensorflow as tf


def distance(vec1, vec2):
    d = 0
    assert(np.shape(vec1[0]) == np.shape(vec2[0]))
    for i in range(np.shape(vec1[0])[0]):
        d += abs(vec1[0][i] - vec2[0][i])
    return d


def affectation_classe(source_image):
    im = facedetection_and_resizing(source_image)
    model = VGGFace(model='resnet50', include_top=False,
                    input_shape=(224, 224, 3), pooling='avg')
    min_d = None
    personne_predite = None
    if type(im) != type(None):
        print('dans le if')
        im_tensor = tf.convert_to_tensor(im)
        im_tensor = np.expand_dims(im_tensor, axis=0)
        preds = model.predict(im_tensor)
        for personne in dic.keys():
            vecteur = dic[personne]
            dist = distance(vecteur, preds)
            if type(min_d) == type(None) or dist < min_d:
                min_d = dist
                personne_predite = personne
    # print(personne_predite)
    return personne_predite
