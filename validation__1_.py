from imagecollect.recognition_image import affectation_classe
import os


def test_performance_base_de_donnees(nom_bdd):
    for personne in os.walk(f'../{nom_bdd}'):
        if personne[0] != '.' and personne[0] != f'../{nom_bdd}':
            p_pas_rec = 0
            bien_rec = 0
            for image in os.listdir(f'{personne[0]}'):
                pp = affectation_classe(f'{personne[0]}/{image}')
                if type(pp) == type(None):
                    p_pas_rec += 1
                if pp == f'{personne[0]}':
                    bien_rec += 1
            print(personne[0])
            print('Il y a eu %d images pas reconnues' % (p_pas_rec))
            print('Il y a eu %d pourcents de bonnes réponses' %
                  ((bien_rec + p_pas_rec) * 5))


def test_performance_nouvelle_image(source_image):
    pred = affectation_classe(source_image)
    print(pred)
    return pred


# Résultats du test de performances pour le dataset fait à la main

# ../dataset/angele
# Il y a eu 5 images pas reconnues
# Il y a eu 100 pourcents de bonnes réponses
# ../dataset/emmanuel_macron
# Il y a eu 2 images pas reconnues
# Il y a eu 100 pourcents de bonnes réponses
# ../dataset/david_pujadas
# Il y a eu 0 images pas reconnues
# Il y a eu 100 pourcents de bonnes réponses
# ../dataset/nicolas_sarkozy
# Il y a eu 3 images pas reconnues
# Il y a eu 100 pourcents de bonnes réponses
# ../dataset/keen_v
# Il y a eu 5 images pas reconnues
# Il y a eu 100 pourcents de bonnes réponses
# ../dataset/jean_lassalle
# Il y a eu 1 images pas reconnues
# Il y a eu 100 pourcents de bonnes réponses
# ../dataset/dua_lipa
# Il y a eu 3 images pas reconnues
# Il y a eu 100 pourcents de bonnes réponses
# ../dataset/kenji_girac
# Il y a eu 3 images pas reconnues
# Il y a eu 100 pourcents de bonnes réponses
# ../dataset/louis_de_funes
# Il y a eu 5 images pas reconnues
# Il y a eu 95 pourcents de bonnes réponses
# ../dataset/wejdene
# Il y a eu 8 images pas reconnues
# Il y a eu 100 pourcents de bonnes réponses
# ../dataset/francois_hollande
# Il y a eu 2 images pas reconnues
# Il y a eu 100 pourcents de bonnes réponses
# ../dataset/aya_nakamura
# Il y a eu 5 images pas reconnues
# Il y a eu 100 pourcents de bonnes réponses
