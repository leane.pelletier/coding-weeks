import numpy as np

# Quand on exécute main, on utilise la ligne suivante :
from imagecollect.facedetection import facedetection_and_resizing

# Quand on exécute le fichier en tant que tel, on utilise la ligne suivante :
#from facedetection import facedetection_and_resizing
import cv2
from keras_vggface import VGGFace
import os
import tensorflow as tf


def database_building_and_update(filename, nom_bdd):
    data = open(filename, 'r').readlines()
    dossiers_ex = os.listdir(f'../{nom_bdd}')
    for d in data:
        if d not in dossiers_ex:
            os.mkdir(f"../{nom_bdd}/{d}")


# os.mkdir(f"../dataset/{d}")

# Ceci est le dictionnaire avec les embeddings.
dic = {}

# Si on appelle avec run_without_debugging on utilise cette fonction :


def build_file_embeddings2(nom_bdd):
    model = VGGFace(model='resnet50', include_top=False,
                    input_shape=(224, 224, 3), pooling='avg')
    # os.chdir(f'../{nom_bdd}')
    for personne in os.walk(f'../{nom_bdd}'):
        print(personne)
        if personne[0] != '.' and personne[0] != f'../{nom_bdd}':
            l_embeddings = []
            for image in os.listdir(f'{personne[0]}'):
                im = facedetection_and_resizing(
                    f'{personne[0]}/{image}')
                if type(im) != type(None):
                    im_tensor = tf.convert_to_tensor(im)
                    im_tensor = np.expand_dims(im_tensor, axis=0)
                    preds = model.predict(im_tensor)
                    l_embeddings.append(preds)
            s = 0
            for el in l_embeddings:
                s += el
            embedding = s/len(l_embeddings)
            dic[personne[0]] = embedding

# Si on appelle avec python3 etc. on utilise cette fonction


def build_file_embeddings(nom_bdd):
    model = VGGFace(model='resnet50', include_top=False,
                    input_shape=(224, 224, 3), pooling='avg')
    # os.chdir(f'../{nom_bdd}')
    for personne in os.walk(f'{nom_bdd}'):
        if personne[0] != '.' and personne[0] != f'{nom_bdd}':
            l_embeddings = []
            for image in os.listdir(f'{personne[0]}'):
                im = facedetection_and_resizing(
                    f'{personne[0]}/{image}')
                if type(im) != type(None):
                    im_tensor = tf.convert_to_tensor(im)
                    im_tensor = np.expand_dims(im_tensor, axis=0)
                    preds = model.predict(im_tensor)
                    l_embeddings.append(preds)
            s = 0
            for el in l_embeddings:
                s += el
            embedding = s/len(l_embeddings)
            dic[personne[0]] = embedding


# build_file_embeddings('dataset')
# print(dic)
