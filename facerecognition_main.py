from imagecollect.database_building_and_update import build_file_embeddings
from imagecollect.recognition_image import affectation_classe
import os
import argparse

# Corps du programme :

# Le chargement des données est préfait : il s'agit ou bien du dossier lfw (pris sur internet), ou bien du dossier dataset (fait à la main)
# L'appel à build_file_embeddings (du module imagecollect, fichier database_building_and_update.py) remplit le dictionnaire avec les embeddings.
# De plus, on y fait le prétraitement des images.
# Puis, à partir d'une image test, ici 'dataset/angele/img_2.png', on appelle affectation_classe.
# Cette fonction effectue le prétraitement puis effectue une prédiction.


def main(bdd, source_image):
    build_file_embeddings(bdd)
    pred = affectation_classe(source_image)
    print(pred)

# Par défaut :
# bdd = 'dataset'
# source_image = 'dataset/angele/img_2.png'


# if __name__ == '__main__':
#    build_file_embeddings('dataset')
#    pred = affectation_classe('dataset/angele/img_2.png')
#    print(pred)

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-b", "--bdd", required=True,
                    help="base de données que vous voulez utiliser")
    ap.add_argument("-i", "--input", required=True, help="path to input image")
    args = vars(ap.parse_args())
    bdd = args["bdd"]
    source_image = args["input"]

    main(bdd, source_image)
    # main('dataset','dataset/angele/img_2.png')
